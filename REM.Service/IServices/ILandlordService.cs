﻿using REM.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace REM.Service.IServices
{
    public interface ILandlordService
    {
        Landlord Get(long id);

        IEnumerable<Landlord> GetAll();

        IEnumerable<Landlord> Where(Expression<Func<Landlord, bool>> condition, string[] includeTables = null);

        Landlord Insert(Landlord landlord);

        void Update(Landlord landlord);

        void Delete(long id);
    }
}