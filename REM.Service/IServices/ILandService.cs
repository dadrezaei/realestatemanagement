﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using REM.Data.Models;

namespace REM.Service.IServices
{
    public interface ILandService
    {
        IEnumerable<Land> GetAll();

        Land Get(long id);

        IEnumerable<Land> Where(Expression<Func<Land, bool>> condition, string[] includeTables = null);

        Land Insert(Land land);

        void Update(Land land);

        void Delete(long id);
    }
}