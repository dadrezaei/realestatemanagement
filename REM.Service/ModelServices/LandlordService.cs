﻿using REM.Data.Models;
using REM.Repo.Repositories;
using REM.Service.IServices;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace REM.Service.ModelServices
{
    public class LandlordService : ILandlordService
    {
        private IRepository<Landlord> landlordRepository;

        public LandlordService(IRepository<Landlord> landlordRepository)
        {
            this.landlordRepository = landlordRepository;
        }

        public Landlord Get(long id)
        {
            return landlordRepository.Get(id);
        }

        public IEnumerable<Landlord> GetAll()
        {
            return landlordRepository.GetAll();
        }

        public IEnumerable<Landlord> Where(Expression<Func<Landlord, bool>> condition, string[] includeTables = null)
        {
            return landlordRepository.Where(condition, includeTables);
        }

        public Landlord Insert(Landlord landlord)
        {
            return landlordRepository.Insert(landlord);
        }

        public void Update(Landlord landlord)
        {
            landlordRepository.Update(landlord);
        }

        public void Delete(long id)
        {
            Landlord landlord = Get(id);
            landlordRepository.Delete(landlord);
            landlordRepository.SaveChanges();
        }

    }
}