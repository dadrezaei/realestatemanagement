﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using REM.Data.Models;
using REM.Repo.Repositories;
using REM.Service.IServices;

namespace REM.Service.ModelServices
{
    public class LandService : ILandService
    {
        private IRepository<Land> landRepository;

        public LandService(IRepository<Land> landRepository)
        {
            this.landRepository = landRepository;
        }

        public Land Get(long id)
        {
            return landRepository.Get(id);
        }

        public IEnumerable<Land> GetAll()
        {
            return landRepository.GetAll();
        }

        public IEnumerable<Land> Where(Expression<Func<Land, bool>> condition, string[] includeTables = null)
        {
            return landRepository.Where(condition, includeTables);
        }

        public Land Insert(Land land)
        {
            return landRepository.Insert(land);
        }

        public void Update(Land land)
        {
            landRepository.Update(land);
        }

        public void Delete(long id)
        {
            Land land = Get(id);
            landRepository.Delete(land);
            landRepository.SaveChanges();
        }
    }
}