﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using REM.Data.BaseModels;
using REM.Repo.AppContext;

namespace REM.Repo.Repositories
{
    public class Repository<T> : IRepository<T> where T : BaseEntity
    {
        private readonly RemDbContext _remDbContext;
        private DbSet<T> dbContext;

        public Repository(RemDbContext context)
        {
            _remDbContext = context;
            dbContext = context.Set<T>();
        }
        
        public T Get(long id)
        {
            return dbContext.SingleOrDefault(s => s.ID == id && !s.IsDelete);
        }

        public IEnumerable<T> GetAll()
        {
            return dbContext.AsEnumerable().Where(x => !x.IsDelete);
        }

        public IEnumerable<T> Where(Expression<Func<T, bool>> predicate, string[] includeTables = null)
        {
            IQueryable<T> result = _remDbContext.Set<T>().Where(predicate).Where(x => !x.IsDelete);
            if (includeTables == null) return result.AsEnumerable();

            result = includeTables.Aggregate(result, (current, includeTable) => current.Include(includeTable));
            return result.AsEnumerable();
        }

        public T Insert(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            var dtNow = DateTime.Now;
            entity.CreateDate = dtNow;
            entity.UpdateDate = dtNow;
            var model = dbContext.Add(entity);
            _remDbContext.SaveChanges();
            return model.Entity;
        }

        public void InsertAll(IEnumerable<T> entities)
        {
            if (entities is null || !entities.Any())
            {
                throw new ArgumentNullException("entity");
            }
            var dtNow = DateTime.Now;
            foreach (var entity in entities)
            {
                entity.CreateDate = dtNow;
                entity.UpdateDate = dtNow;
            }
            dbContext.AddRange(entities);
            _remDbContext.SaveChanges();
        }

        public void Update(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entity.UpdateDate = DateTime.Now;
            _remDbContext.SaveChanges();
        }

        public void Delete(T entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("entity");
            }
            entity.IsDelete = true;
            Update(entity);
        }

        public void SaveChanges()
        {
            _remDbContext.SaveChanges();
        }
    }
}