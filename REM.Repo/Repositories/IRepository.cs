﻿using REM.Data.BaseModels;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace REM.Repo.Repositories
{
    public interface IRepository<T> where T : BaseEntity
    {
        T Get(long id);
        IEnumerable<T> GetAll();
        IEnumerable<T> Where(Expression<Func<T, bool>> condition, string[] includeTables = null);
        T Insert(T entity);
        void InsertAll(IEnumerable<T> entities);
        void Update(T entity);
        void Delete(T entity);
        void SaveChanges();
    }
}