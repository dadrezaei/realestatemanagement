﻿using Microsoft.EntityFrameworkCore;
using REM.Data.Models;

namespace REM.Repo.AppContext
{
    public class RemDbContext : DbContext
    {
        public RemDbContext(DbContextOptions<RemDbContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Landlord> Landlords { get; set; }

        public virtual DbSet<Land> Lands { get; set; }

    }
}