﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using REM.Data.Models;
using REM.Service.IServices;
using REM.Web.Models;

namespace REM.Web.Controllers
{
    public class LandController : Controller
    {
        private readonly ILandService _landService;
        private readonly ILandlordService _landlordService;

        public LandController(ILandlordService landlordService, ILandService landService)
        {
            _landlordService = landlordService;
            _landService = landService;
        }

        // GET: LandController
        public ActionResult Index()
        {
            List<LandViewModel> model = new List<LandViewModel>();
            var landList = _landService.Where(x=>true,new[] {"Landlord"}).ToList();
            model = landList.Select(x => new LandViewModel()
            {
                LandlordId = x.Landlord?.ID ?? 0,
                Area = x.Area,
                IsNorthern = x.IsNorthern,
                LandAddress = x.Address ?? "",
                LandCode = x.Code ?? "",
                LandId = x.ID,
                LandName = x.Name,
                LandlordCellphone = x.Landlord?.Cellphone ?? "",
                LandlordName = x.Landlord?.FirstName + " " + x.Landlord?.LastName
            }).ToList();
            return View(model);
        }

        // GET: LandController/Details/5
        public ActionResult Details(int id)
        {
            var land = _landService.Where(x => x.ID == id, new[] { "Landlord" }).SingleOrDefault();
            LandViewModel model = new LandViewModel();
            model.Fill(land);
            var landlordList = _landlordService.GetAll().ToList();
            model.LandlordItems = landlordList.Select(x => new SelectListItem
            {
                Text = x.FirstName + " " + x.LastName,
                Value = x.ID.ToString()
            }).ToList();

            return View(model);
        }

        // GET: LandController/Create
        public ActionResult Create()
        {
            var landlordList = _landlordService.GetAll().ToList();
            LandViewModel model = new LandViewModel
            {
                LandlordItems = landlordList.Select(x => new SelectListItem
                {
                    Text = x.FirstName + " " + x.LastName,
                    Value = x.ID.ToString()
                }).ToList()
            };
            return View(model);
        }

        // POST: LandController/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(LandViewModel model)
        {
            try
            {
                Land land = model.Fill();
                var landlord = _landlordService.Get(model.LandlordId);
                land.Landlord = landlord;
                _landService.Insert(land);
                return new JsonResult(new
                {
                    code = 200,
                   location = ""
                });
            }
            catch (Exception exception)
            {
                return new JsonResult(new
                {
                    code = 1,
                    message = "خطا در پردازش درخواست"
                });
            }
        }

        // GET: LandController/Edit/5
        public ActionResult Edit(int id)
        {
            var land = _landService.Where(x => x.ID == id, new[] { "Landlord" }).SingleOrDefault();
            LandViewModel model = new LandViewModel();
            model.Fill(land);
            var landlordList = _landlordService.GetAll().ToList();
            model.LandlordItems = landlordList.Select(x => new SelectListItem
            {
                Text = x.FirstName + " " + x.LastName,
                Value = x.ID.ToString()
            }).ToList();

            return View(model);
        }

        // POST: LandController/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit(int id,LandViewModel model)
        {
            try
            {
                var land = _landService.Get(id);
                land.Address = model.LandAddress;
                land.Area = model.Area;
                land.Code = model.LandCode;
                land.IsNorthern = model.IsNorthern;
                land.Name = model.LandName;
                _landService.Update(land);
                return new JsonResult(new
                {
                    code = 200,
                    location = ""
                });
            }
            catch
            {
                return new JsonResult(new
                {
                    code = 1,
                    message = "خطا در پردازش درخواست"
                });
            }
        }

        // GET: LandController/Delete/5
        public ActionResult Delete(int id)
        {
            var land = _landService.Where(x => x.ID == id, new[] { "Landlord" }).SingleOrDefault();
            LandViewModel model = new LandViewModel();
            model.Fill(land);
            var landlordList = _landlordService.GetAll().ToList();
            model.LandlordItems = landlordList.Select(x => new SelectListItem
            {
                Text = x.FirstName + " " + x.LastName,
                Value = x.ID.ToString()
            }).ToList();

            return View(model);
        }

        // POST: LandController/Delete/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Delete(int id, LandViewModel model)
        {
            try
            {
                _landService.Delete(id);
                return RedirectToAction(nameof(Index));
            }
            catch
            {
                return View(model);
            }
        }
    }
}
