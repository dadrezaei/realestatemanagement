﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Mvc.Rendering;
using REM.Data.Models;

namespace REM.Web.Models
{
    public partial class LandViewModel
    {
        public LandViewModel()
        {
            LandlordItems = new List<SelectListItem>();
        }

        public long LandId { get; set; }

        [Required(ErrorMessage = "وارد کردن {0} الزامی است")]
        [Display(Name = "نام ملک")]
        public string LandName { get; set; }

        [Display(Name = "کد ملک")]
        public string LandCode { get; set; }

        [Display(Name = "نشانی ملک")]
        public string LandAddress { get; set; }

        [Display(Name = "مساحت")]
        public int Area { get; set; }

        [Display(Name = "ملک شمالی است")]
        public bool IsNorthern { get; set; }

        [Display(Name = "نام مالک")]
        public string LandlordName { get; set; }

        [Display(Name = "تلفن مالک")]
        public string LandlordCellphone { get; set; }

        public List<SelectListItem> LandlordItems { get; set; }

        [Display(Name = "نام مالک")]
        [Required(ErrorMessage = "وارد کردن {0} الزامی است")]
        [Range(1, int.MaxValue)]
        public long LandlordId { get; set; }
    }

    public partial class LandViewModel
    {
        public Land Fill()
        {
            return new Land
            {
                Address = LandAddress,
                Area = Area,
                Code = LandCode,
                IsNorthern = IsNorthern,
                Name = LandName,
            };
        }

        public void Fill(Land land)
        {
            LandId = land.ID;
            LandAddress = land.Address;
            Area = land.Area;
            LandCode = land.Code;
            IsNorthern = land.IsNorthern;
            LandName = land.Name;
            LandlordName = land.Landlord?.FirstName + " " + land.Landlord?.LastName;
            LandlordCellphone = land.Landlord?.Cellphone;
            LandlordId = land.Landlord?.ID ?? 0;
        }
    }
}