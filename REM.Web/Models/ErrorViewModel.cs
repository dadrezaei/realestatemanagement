using System;
using Newtonsoft.Json;

namespace REM.Web.Models
{
    public class ErrorViewModel
    {
        public int Code { get; set; }
       
        public string Message { get; set; }

        public override string ToString()
        {
            return JsonConvert.SerializeObject(this);
        }
    }
}
