﻿function submitForm(formId) {
    try {

        var form = $("#" + formId)[0];
        var formData = new FormData(form);
        var isValid = $("#" + formId).valid();

        if (isValid === true) {

            $.ajax({
                type: "POST",
                url: $("#" + formId).attr("action"),
                data: formData,
                processData: false,
                contentType: false,
                success: function (data, status, xhr) {
                    responseSuccess(data);
                },
                failure: function (data, status, xhr) {
                    responseFailure(data);
                }
            });
        }

    } catch (e) {
        alert(e.message);
    }
}

function responseSuccess(data) {
    if (data.code !== 200) {
        alert(data.message);
    }
    else {
        window.location.href = window.location.origin + "/" + data.location;
    }
}

function responseFailure(data) {
    alert(data.message);
}
