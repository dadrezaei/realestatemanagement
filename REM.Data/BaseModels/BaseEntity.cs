﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace REM.Data.BaseModels
{
    public abstract class BaseEntity
    {
        public long ID { get; set; }

        [DefaultValue(false)]
        public bool IsDelete { get; set; }

        public DateTime CreateDate { get; set; }

        //[Display(Name = "آخرین بروزرسانی")]
        public DateTime UpdateDate { get; set; }
    }
}
