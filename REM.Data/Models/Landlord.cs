﻿using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using REM.Data.BaseModels;

namespace REM.Data.Models
{
    public class Landlord : BaseEntity
    {
        [Required]
        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        public string Cellphone { get; set; }

        public virtual ICollection<Land> Lands { get; set; }
    }
}