﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using REM.Data.BaseModels;

namespace REM.Data.Models
{
    public class Land : BaseEntity
    {
        [MaxLength(50)]
        public string Name { get; set; }
        
        [MaxLength(50)]
        public string Code { get; set; }

        public string Address { get; set; }
        
        public int Area { get; set; }
        
        public bool IsNorthern { get; set; }

        [Required]
        public virtual Landlord Landlord { get; set; }
    }
}